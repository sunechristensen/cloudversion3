<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  


</head>
<body>

<form id="usertable" method="post"></form>
<table>
	<tr>
		<th>Click on user to delete</th>
	</tr>
	<c:forEach var="username" items="${usernames}">
		<tr>
			<td><a href="/usertables?username=${username}">${username}</a></td>
		</tr>
	</c:forEach>
</table>
</body>
</html>