package loginprojekt;

import java.io.Serializable;

public class User implements Serializable{
	
private String userName;
private String password; //Hidden in plain sight
private String fullname;

public User(String userName, String password, String fullname) {
	this.userName = userName;
	this.password = password;
	this.fullname = fullname;
}

public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

public String getFullname() {
	return fullname;
}


public void setFullname(String fullname) {
	this.fullname = fullname;
}


@Override
	public String toString() {
		return this.userName + " " + this.password + " " + this.fullname;
	}

}
