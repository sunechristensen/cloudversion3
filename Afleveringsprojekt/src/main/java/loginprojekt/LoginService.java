package loginprojekt;

//////////////////////////////////////////////////
//various imports
//////////////////////////////////////////////////
////import com.google.auth.Credentials;
import com.google.api.gax.paging.Page;
import com.google.cloud.ReadChannel;
import com.google.cloud.WriteChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
//////////////////////////////////////////////////
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Scanner;

import loginprojekt.User;

public class LoginService {

	/*
	 * Reads userStorage file and checks for a username and password match. All is
	 * done in plain text
	 * 
	 */

	// TODO BUG: The last line in userStorage cannot be empty!
	public boolean authenticate(String usernameToCheck, String passwordToCheck) throws FileNotFoundException {

		File file = new File("./WEB-INF/userStorage.txt");
		Scanner sc = new Scanner(file);
		String st = "";

		while (sc.hasNextLine()) {
			sc.skip(""); // skips empty lines in the storage-file
			st = sc.next();
			String[] info = st.split(";");
			System.out.println(usernameToCheck + " " + passwordToCheck);
			System.out.println(info[0]);
			if (info[0].equalsIgnoreCase(usernameToCheck) && info[1].equals(passwordToCheck)) {
				sc.close();
				return true;
			}
		}
		sc.close();
		return false;
	}

	/*
	 * Gets a user based on username
	 */
	public User getUser(String username) throws FileNotFoundException {
		File file = new File("./WEB-INF/userStorage.txt");
		Scanner sc = new Scanner(file);
		String st = "";

		while (sc.hasNextLine()) {
			sc.skip(""); // skips empty lines in the storage-file
			st = sc.next();
			String[] info = st.split(";");

			if (info[0].equalsIgnoreCase(username)) {
				User user = new User(info[0], info[1], info[2]);
				sc.close();
				return user;
			}
		}
		sc.close();
		return null;
	}

	// TODO On the cloud, the userStorage is set to read-only. Change that.
	/*
	 * Creates a user and appends it to the userStorage file. Checks if a user with
	 * the same username already exist
	 */
	public User createUser(String username, String password, String fullname) throws IOException {
		if (this.getUser(username) != null) {
			User user = new User(username, password, fullname);
			FileWriter write = new FileWriter("./WEB-INF/userStorage.txt", true);
			PrintWriter pw = new PrintWriter(write);
			pw.println("\n" + username + ";" + password + ";" + fullname);
			pw.close();
			return user;
		} else {
			return null;
		}
	}
}
